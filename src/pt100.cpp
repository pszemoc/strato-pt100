#include "pt100.hpp"
//#include <wiringPiSPI.h>
#include <thread>
#include <chrono>

#include "spdlog/spdlog.h"

pt100_t::pt100_t() {
//  wiringPiSPISetup(500000, 0);
  spdlog::info("pt100 constructor");
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  uint8_t val = read_register8(pt100_reg::config);
  spdlog::info("Config reg: {}", (int)val);
  write_register8(pt100_reg::config, 0b00010010);
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  val = read_register8(pt100_reg::config);
  spdlog::info("Config reg: {}", (int)val);

  set_wires(3);
  enable_bias(false);
  auto_convert(false);
  clear_fault();
}

pt100_t::~pt100_t() {}

void pt100_t::clear_fault() {
  uint8_t t = read_register8(pt100_reg::config);
  t &= ~0x2C;
  t |= pt100_reg::config_fault_clear;
  write_register8(pt100_reg::config, t);
}

uint16_t pt100_t::read_rtd() {
  clear_fault();
  enable_bias(true);
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  uint8_t t = read_register8(pt100_reg::config);
  t |= pt100_reg::config_one_shot;
  write_register8(pt100_reg::config, t);
  std::this_thread::sleep_for(std::chrono::milliseconds(65));

  uint16_t rtd = read_register16(pt100_reg::temperature_msb);

  rtd = rtd >> 1;

  return rtd;
}

void pt100_t::set_wires(uint8_t wires) {
  uint8_t t = read_register8(pt100_reg::config);
  if (wires == 3) {
    t |= pt100_reg::config_3_wire;
  } else if (wires == 2 || wires == 4) {
    t &= ~pt100_reg::config_3_wire;
  }
  write_register8(pt100_reg::config, t);
}

void pt100_t::auto_convert(bool b) {
  uint8_t t = read_register8(pt100_reg::config);
  if (b) {
    t |= pt100_reg::config_auto_convert;      
  } else {
    t &= ~pt100_reg::config_auto_convert;     
  }
  write_register8(pt100_reg::config, t);

}

void pt100_t::enable_bias(bool b) {
  uint8_t t = read_register8(pt100_reg::config);
  if (b) {
    t |= pt100_reg::config_bias;      
  } else {
    t &= ~pt100_reg::config_bias;     
  }
  write_register8(pt100_reg::config, t);
}

uint8_t pt100_t::read_register8(uint8_t addr) {
  uint8_t buf[2];
  buf[0] = addr;
//  wiringPiSPIDataRW(0, buf, 2);
  return buf[1];
}  

uint16_t read_register16(uint8_t addr) {
  uint8_t buf[3];
  buf[0] = addr;
//  wiringPiSPIDataRW(0, buf, 2);
  uint16_t res = buf[1] << 8 | buf[0];
  return res;
}
  
void pt100_t::write_register8(uint8_t addr, uint8_t val) {
  uint8_t buf[2];
  buf[0] = addr | 0x80;
  buf[1] = val;
//  wiringPiSPIDataRW(0, buf, 2);
}

float pt100_t::read_temperature(double rtd_nominal, double ref_resistor) {
  double z1, z2, z3, z4, rt, temp;
  double rtd_a = 3.9083e-3;
  double rtd_b = -5.775e-7;

  rt = read_rtd();
  rt /= 32768;
  rt *= ref_resistor;


  z1 = -rtd_a;
  z2 = rtd_a * rtd_a - (4 * rtd_b);
  z3 = (4 * rtd_b) / rtd_nominal;
  z4 = 2 * rtd_b;

  temp = z2 + (z3 * rt);
  temp = (sqrt(temp) + z1) / z4;
  
  if (temp >= 0) return temp;

  rt /= rtd_nominal;
  rt *= 100;  

  double rpoly = rt;

  temp = -242.02;
  temp += 2.2228 * rpoly;
  rpoly *= rt;  
  temp += 2.5859e-3 * rpoly;
  rpoly *= rt;  
  temp -= 4.8260e-6 * rpoly;
  rpoly *= rt;  
  temp -= 2.8183e-8 * rpoly;
  rpoly *= rt;  
  temp += 1.5243e-10 * rpoly;

  return temp;
}