CXX		  := g++
CXX_FLAGS := -Wall -Wextra -std=c++17 -ggdb

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:= -lzmq -lconfig++ -lwiringPi
EXECUTABLE	:= main


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp
	mkdir -p bin
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*
	
install:
	sudo mkdir -p /var/log/balloon
	sudo cp ./$(BIN)/$(EXECUTABLE) /usr/bin/sp-pt100
	sudo cp sp-pt100.service /etc/systemd/system

uninstall:
	sudo rm /usr/bin/sp-pt100 /etc/systemd/system/sp-pt100.service

