#pragma once
#include <stdint.h>

namespace pt100_reg {
  constexpr int config          = 0x00;
  constexpr int temperature_msb = 0x01;
  constexpr int temperature_lsb = 0x02;

  constexpr int config_bias             = (1 << 7);
  constexpr int config_auto_convert     = (1 << 6);
  constexpr int config_one_shot         = (1 << 5);
  constexpr int config_3_wire           = (1 << 4);
  constexpr int config_fault_clear      = (1 << 1);
}

class pt100_t {
private:
  uint8_t read_register8(uint8_t addr);
  uint16_t read_register16(uint8_t addr);
  void    write_register8(uint8_t addr, uint8_t val);

  void clear_fault();
  uint16_t read_rtd();

  void set_wires(uint8_t wires);
  void auto_convert(bool b);
  void enable_bias(bool b);


public:
  pt100_t();
  ~pt100_t();

  float read_temperature(double rtd_nominal, double ref_resistor);  // [deg C]
};